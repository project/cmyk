<?php
// $Id: template.php,v 1.13 2010/12/14 01:04:27 dries Exp $

/**
 * Override or insert variables into the node template.
 */
function cmyk_preprocess_node(&$variables) {
  $variables['submitted'] = t('published on !datetime', array('!datetime' => $variables['date']));
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}
